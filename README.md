# WPF+CefSharp 实现将web页面嵌入 windows桌面程序
>> CefSharp是封装了ChromiumWebBrowser的一个.net开源项目，该项目替代了.net本身的WebBrowser控件,并且实现了C#与javascript之间的交互方法，目前支持Winform和WPF,
建议使用WPF会好一些，个人感觉哦！

## 创建项目就不多说了，直接上图

 ![将CefSharp导入到项目中](https://gitee.com/uploads/images/2017/1214/172913_3cac2689_562713.png "屏幕截图.png")

### 然后MainWindow.xaml中创建 grid_loaded

```
private void Grid_Loaded(object sender, RoutedEventArgs e){
      var setting = new CefSettings();
      Cef.Initialize(setting);
                
      var web = new ChromiumWebBrowser(){
          Address = "http://www.baidu.com/"
      };

      WBGrid.Children.Add(web); 
}
```
### 点击启动就OK啦！（一定要注意，只能在X86平台下进行调试哦！）
