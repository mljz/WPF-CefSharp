﻿using CefSharp.Wpf;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefSharp;

namespace WpfApp3
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        IBrowserHost host;
        bool IsMouseDown = false;
        Point? LastSavedPoint = null;
        IWebBrowser browser;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // 设置全屏
            WindowState = WindowState.Normal;
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            Topmost = true;

            Left = 0.0;
            Top = 0.0;
            Width = SystemParameters.PrimaryScreenWidth;
            Height = SystemParameters.PrimaryScreenHeight;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            EventLog log = new EventLog();
            try
            {
                var setting = new CefSettings {
                    BrowserSubprocessPath = "CefSharp.BrowserSubprocess.exe"
                };
                setting.CefCommandLineArgs.Add("touch-events","enabled");
                Cef.Initialize(setting);
                
                var web = new ChromiumWebBrowser()
                {
                    Address = "http://www.baidu.com/"
                };
                web.MenuHandler = new MenuHandler();

                WBGrid.Children.Add(web);
                browser = web;
            }
            catch (System.IO.FileNotFoundException exception)
            {
                log.WriteEntry("CefSharp访问：", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// 禁止右键菜单
        /// </summary>
        public class MenuHandler : IContextMenuHandler
        {

            void IContextMenuHandler.OnBeforeContextMenu(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, IMenuModel model)
            {
                model.Clear();
            }

            bool IContextMenuHandler.OnContextMenuCommand(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, CefMenuCommand commandId, CefEventFlags eventFlags)
            {
                return false;
            }

            void IContextMenuHandler.OnContextMenuDismissed(IWebBrowser browserControl, IBrowser browser, IFrame frame)
            {
            }

            bool IContextMenuHandler.RunContextMenu(IWebBrowser browserControl, IBrowser browser, IFrame frame, IContextMenuParams parameters, IMenuModel model, IRunContextMenuCallback callback)
            {
                return false;
            }
        }

        private void WBGrid_TouchMove(object sender, TouchEventArgs e)
        {
            if (host != null && IsMouseDown)
            {
                TouchPoint t = e.GetTouchPoint(this);
                Point p = t.Position;

                if (!LastSavedPoint.HasValue)
                {
                    LastSavedPoint = p;
                }

                int x = (int)p.X;
                int y = (int)p.Y;
                int oldX = (int)LastSavedPoint.Value.X;
                int oldy = (int)LastSavedPoint.Value.Y;

                host.SendMouseWheelEvent(x, y, 0, y - oldy, CefEventFlags.MiddleMouseButton);

                LastSavedPoint = p;
            }
        }

        private void WBGrid_TouchLeave(object sender, TouchEventArgs e)
        {
            IsMouseDown = false;
            LastSavedPoint = null;
        }

        private void WBGrid_TouchDown(object sender, TouchEventArgs e)
        {
            if (host == null)
            {
                IWebBrowser host2 = browser;
                IBrowser bworser = host2.GetBrowser();
                host = bworser.GetHost();
            }
            IsMouseDown = true;
        }

        private void WBGrid_TouchUp(object sender, TouchEventArgs e)
        {
            IsMouseDown = false;
            LastSavedPoint = null;
        }
    }
}
